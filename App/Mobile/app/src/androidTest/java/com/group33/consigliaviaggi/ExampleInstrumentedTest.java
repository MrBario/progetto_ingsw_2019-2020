package com.group33.consigliaviaggi;

import android.content.Context;
import android.content.ContextWrapper;
import android.view.ContextMenu;

import com.group33.consigliaviaggi.model.dao.VisitatoreDao;
import com.group33.consigliaviaggi.model.entity.Visitatore;
import com.group33.consigliaviaggi.model.factory.VisitatoreDaoFactory;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    // Context of the app under test.
    Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

    VisitatoreDao dao = VisitatoreDaoFactory.getDAOInstance(appContext).getDAO();
    List<Visitatore> list;

    /**
     * Risultati corretti per il record con id "2"
     */
    Integer id = Integer.valueOf(2);
    String nome = "Dario";
    String cognome = "Leone";
    String nascita = "1994-08-07";
    String sesso = "M";
    String nick = "MrBario";
    String email = "barius.leone@gmail.com";

    /**
     * Connessione al database e recuper dati.
     */
    @Before
    public void setup() {
        try {
            list = dao.login(email, "banana");

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Confronto i dati recuperati con i risultati che dovrei avere.
     */

    @Test
    public void useAppContext() {

        assertEquals("com.group33.consigliaviaggi", appContext.getPackageName());
    }

    @Test
    public void testNome() {

        assertEquals(nome, list.get(0).getNome());
    }

    @Test
    public void testCognome() {

        assertEquals(cognome, list.get(0).getCognome());
    }

    @Test
    public void testNascita() {

        assertEquals(nascita, list.get(0).getNascita());
    }

    @Test
    public void testSesso() {

        assertEquals(sesso, list.get(0).getSesso());
    }

    @Test
    public void testNick() {

        assertEquals(nick, list.get(0).getNick());

    }

    @Test
    public void testEmail() {

        assertEquals(email, list.get(0).getEmail());
    }

}
