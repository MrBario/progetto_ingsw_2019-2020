package com.group33.consigliaviaggi.util;

import java.sql.*;

import android.os.StrictMode;
import android.util.Log;

import oracle.jdbc.rowset.OracleCachedRowSet;


public class DBUtil {
    private static final String DEFAULT_DRIVER = "oracle.jdbc.driver.OracleDriver";
    //TODO: Modificare ip, numero porta e nome servizio / SID all'occorrenza
    private static final String DEFAULT_URL = "jdbc:oracle:thin:@192.168.0.102:1337:xe";
    private static final String DEFAULT_USERNAME = "SYSTEM";
    private static final String DEFAULT_PASSWORD = "oracle";
    private static final String TAG = "DBUtil";

    private static Connection connection = null;

    private static void setStrictMode() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static Connection createConnection(String driver, String url, String username, String password) throws ClassNotFoundException, SQLException {
        Class.forName(driver);
        setStrictMode();
        connection = DriverManager.getConnection(url, username, password);
        return connection;
    }

    public static Connection createConnection() throws ClassNotFoundException, SQLException {
        return createConnection(DEFAULT_DRIVER, DEFAULT_URL, DEFAULT_USERNAME, DEFAULT_PASSWORD);
    }

    public static boolean isConnectionOpen() throws SQLException {
        return connection != null && !connection.isClosed();
    }

    public static void dbDisconnect() throws SQLException{
        try{
            if(isConnectionOpen()){
                connection.close();
            }
        }
        catch(Exception e){
            throw e;
        }
    }

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        if (!isConnectionOpen()) {
            createConnection();
        }
        return connection;
    }

    // questo per insert/delete/update
    public static void dbExcecuteQuery(String sqlStmt) throws SQLException, ClassNotFoundException{
        Statement stmt = null;
        try{
            connection = getConnection();
            stmt = connection.createStatement();
            stmt.executeUpdate(sqlStmt);
        }
        catch(SQLException e){
            Log.e(TAG, "dbExcecute: Errore durante dbExcecuteQuery" + e.getMessage());
            throw e;
        }
        finally{
            if(stmt!=null){
                stmt.close();
            }
            dbDisconnect();
        }
    }

    // questo per la ricerca nel database
    public static ResultSet dbExcecute(String sqlQuery) throws SQLException, ClassNotFoundException{
        Statement stmt = null;
        ResultSet rs = null;
        OracleCachedRowSet crs = new OracleCachedRowSet();

        try{
            connection = getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sqlQuery);
            crs.populate(rs);
        }
        catch(SQLException e){
            Log.e(TAG, "dbExcecute: Errore durante dbExcecute" + e.getMessage());
            throw e;
        }
        finally{
            if(rs != null){
                rs.close();
            }
            if(stmt != null){
                stmt.close();
            }
            dbDisconnect();
        }
        return crs;
    }
}
