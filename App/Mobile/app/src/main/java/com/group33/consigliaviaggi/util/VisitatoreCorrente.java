package com.group33.consigliaviaggi.util;

import com.group33.consigliaviaggi.model.entity.Visitatore;

/**
 * Classe Singleton per tener traccia dell'utente connesso al servizio
 */
public class VisitatoreCorrente {

    private static VisitatoreCorrente instance = null;

    private Visitatore visitatore;
    private boolean logged;

    private VisitatoreCorrente() {
        visitatore = null;
        logged = false;
    }

    public static VisitatoreCorrente getInstance() {
        if (instance == null) {
            instance = new VisitatoreCorrente();
        }
        return instance;
    }

    public boolean isLogged() {
        return logged;
    }

    public void connectUser(Visitatore visitatore) {
        this.visitatore = visitatore;
        logged = true;
    }

    public Visitatore getVisitatore() {
        return visitatore;
    }

    public void disconnectUser() {
        logged = false;
    }
}
