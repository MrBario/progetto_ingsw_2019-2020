package com.group33.consigliaviaggi.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.group33.consigliaviaggi.R;
import com.group33.consigliaviaggi.model.dao.VisitatoreDao;
import com.group33.consigliaviaggi.model.entity.Visitatore;
import com.group33.consigliaviaggi.model.factory.VisitatoreDaoFactory;
import com.group33.consigliaviaggi.util.DBUtil;
import com.group33.consigliaviaggi.util.VisitatoreCorrente;

import java.sql.SQLException;
import java.util.List;

import static com.group33.consigliaviaggi.util.Constants.PERMISSIONS_REQUEST_ENABLE_GPS;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private ImageView userImage;
    private EditText userMail, userPassword;
    private TextView userSigUp;
    private Button login;
    private String mail, password;
    private VisitatoreDao Dao;
    private List<Visitatore> visitatore = null;
    private AsyncRequest loginRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Creazione del dao:
        VisitatoreDaoFactory DF = VisitatoreDaoFactory.getDAOInstance(this);
        Dao = DF.getDAO();
        loginRequest = new AsyncRequest();

        setContentView(R.layout.activity_login);

        // Inizializzo e collego i widgets:
        userImage = findViewById(R.id.userImage);
        userMail = findViewById(R.id.userMail);
        userPassword = findViewById(R.id.userPassword);
        userSigUp = findViewById(R.id.signup_btn);
        login = findViewById(R.id.login_btn);
        initButtons();
    }

    private void initButtons() {
        login.setOnClickListener((v) -> {
            mail = String.valueOf(userMail.getText());
            password = String.valueOf(userPassword.getText());
            if (mail.isEmpty() || password.isEmpty()) {
                buildAlertMessage();
            } else {
                loginRequest.execute();
            }
        });

        userSigUp.setOnClickListener((v) -> {
            Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
            v.getContext().startActivity(intent);
        });
    }

    private void buildAlertMessage() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Login non riuscito, ricontrolla le credenziali")
                .setCancelable(true);
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private class AsyncRequest extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                DBUtil.getConnection();
                Log.d(TAG, "doInBackground: Connessione Stabilita");
                visitatore = Dao.login(mail, password);

            } catch (SQLException e) {
                Log.e(TAG, "doInBackground: Errore con la query -> " + e.getMessage());
                //e.printStackTrace();
                buildAlertMessage();
                return null;
            } catch (ClassNotFoundException e) {
                Log.e(TAG, "doInBackground: Errore con la query -> " + e.getMessage());
                //e.printStackTrace();
                buildAlertMessage();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d(TAG, "onPostExecute: Visitatore trovato " + visitatore.get(0).getNome());
            VisitatoreCorrente visitatoreCorrente = VisitatoreCorrente.getInstance();
            visitatoreCorrente.connectUser(visitatore.get(0));
            finish();
        }
    }

}