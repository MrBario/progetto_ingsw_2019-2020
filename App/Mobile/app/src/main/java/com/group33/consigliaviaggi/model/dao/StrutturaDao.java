package com.group33.consigliaviaggi.model.dao;

import com.group33.consigliaviaggi.model.entity.Recensione;
import com.group33.consigliaviaggi.model.entity.Struttura;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface StrutturaDao {
    /**
     * Ricerca nel range del visitatore Strutture nel DB.
     * @param geoLat Latitudine.
     * @param geoLng Longitudine.
     * @param range raggio di ricerca in metri.
     * @param stelle media recensioni.
     * @return Lista di strutture.
     * @throws SQLException se si verifica un errore di accesso al DB o la struttura non esiste.
     */
    List<Struttura> cerca(double geoLat, double geoLng, double range, int stelle) throws SQLException, ClassNotFoundException;


    /**
     * Popola la lista delle strutture.
     * @param rs ResulSet
     * @return Lista Strutture
     * @throws SQLException se viene chiamato su un ResultSet chiuso.
     */
    List<Struttura> getRecords(ResultSet rs) throws SQLException;
}
