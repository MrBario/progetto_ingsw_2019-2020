package com.group33.consigliaviaggi.model.factory;

import android.content.Context;

import com.group33.consigliaviaggi.model.dao.RecensioneDao;
import com.group33.consigliaviaggi.model.sql.RecensioneDaoSQL;

import java.io.InputStream;
import java.util.Properties;

public class RecensioneDaoFactory {

    String db;

    private static RecensioneDaoFactory theDAO;

    public static synchronized RecensioneDaoFactory getDAOInstance(Context ctx) {
        if (theDAO == null)
            theDAO = new RecensioneDaoFactory(ctx);

        return theDAO;

    }

    private RecensioneDaoFactory(Context ctx) {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = ctx.getAssets().open("config.properties");
            prop.load(input);
            db = prop.getProperty("database");
            System.out.println(db);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public RecensioneDao getDAO() {
        if (db.equals("sql"))
            return new RecensioneDaoSQL();

        return null;
    }
}