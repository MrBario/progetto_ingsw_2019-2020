package com.group33.consigliaviaggi.util;

public class Constants {
    public static final int ERROR_DIALOG_REQUEST = 9001;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 9002;
    public static final int PERMISSIONS_REQUEST_ENABLE_GPS = 9003;
    public static final String ASCENDING_ORDER_REQUEST = "ascending";
    public static final String DESCENDING_ORDER_REQUEST = "descending";
}
