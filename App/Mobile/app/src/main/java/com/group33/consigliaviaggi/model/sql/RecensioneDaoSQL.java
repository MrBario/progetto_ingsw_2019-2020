package com.group33.consigliaviaggi.model.sql;

import com.group33.consigliaviaggi.model.dao.RecensioneDao;
import com.group33.consigliaviaggi.model.entity.Recensione;
import com.group33.consigliaviaggi.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecensioneDaoSQL implements RecensioneDao {
    @Override
    public List<Recensione> cerca(String struttura) throws SQLException, ClassNotFoundException{
        String sql = "SELECT * FROM recensioni WHERE Struttura = " + struttura;
        try{
            ResultSet rs = DBUtil.dbExcecute(sql);
            List<Recensione> list = getRecords(rs);
            return list;
        }catch (SQLException e){
            throw e;
        }
        catch (ClassNotFoundException c){
            throw c;
        }
    }

    @Override
    public List<Recensione> cerca(String struttura, String valutazione) throws SQLException, ClassNotFoundException {

        Pattern pat;
        Matcher matc;
        String sql = "SELECT * FROM recensioni WHERE ";

        if (!(struttura.equals(""))) {
            sql += " Struttura = " + struttura;
        }
        if (!(valutazione.equals(""))) {
            sql += " AND Valutazione = " + valutazione;
        }

        pat = Pattern.compile("where$|and$"); // cancella where o and finali
        matc = pat.matcher(sql);
        sql = matc.replaceAll("");

        try {
            ResultSet rs = DBUtil.dbExcecute(sql);
            List<Recensione> list = getRecords(rs);
            return list;
        } catch (SQLException e) {
            throw e;
        }
        catch(ClassNotFoundException c){
            throw c;
        }
    }

    @Override
    public void inserisci(String visitatore, String struttura, String pubblicazione, String titolo, String testo, String valutazione) throws SQLException, ClassNotFoundException {

        Pattern pat;
        Matcher matc;
        String sql = "INSERT INTO recensioni (";
        String sql2 = "VALUES(";

        if (!(visitatore.equals(""))) {
            sql += "visitatore,";
            sql2 += visitatore + ",";
        }
        if (!(struttura.equals(""))) {
            sql += "struttura,";
            sql2 += struttura + ",";
        }
        if (!(pubblicazione.equals(""))) {
            sql += "pubblicazione,";
            sql2 += "'" + pubblicazione + "',";
        }
        if (!(titolo.equals(""))) {
            sql += "titolo,";
            sql2 += "'" + titolo + "',";
        }
        if (!(testo.equals(""))) {
            sql += "testo,";
            sql2 += "'" + testo + "',";
        }
        if (!(valutazione.equals(""))) {
            sql += "valutazione";
            sql2 += valutazione;
        }


        pat = Pattern.compile(",$"); // cancella virgole finali
        matc = pat.matcher(sql);
        sql = matc.replaceAll("");
        matc = pat.matcher(sql2);
        sql2 = matc.replaceAll("");
        sql += ")" + sql2 + ")";

        try {
            DBUtil.dbExcecuteQuery(sql);
        } catch (SQLException e) {
            throw e;
        }
        catch (ClassNotFoundException c){
            throw c;
        }
    }

    @Override
    public List<Recensione> getRecords(ResultSet rs) throws SQLException {
        try{
            List<Recensione> list = new ArrayList<>();

            if (rs != null) {
                while (rs.next()) {
                    Recensione r = new Recensione();
                    r.setId(rs.getInt("ID_Recensione"));
                    r.setVisitatore(rs.getString("Visitatore"));
                    r.setStruttura(rs.getString("Struttura"));
                    r.setPubblicazione(rs.getString("Pubblicazione"));
                    r.setTitolo(rs.getString("Titolo"));
                    r.setTesto(rs.getString("Testo"));
                    r.setValutazione(rs.getInt("Valutazione"));

                    list.add(r);
                }
            }
            return list;
        }
        catch(SQLException e){
            throw e;
        }
    }
}
