package com.group33.consigliaviaggi.model.dao;

import com.group33.consigliaviaggi.model.entity.Recensione;

import java.sql.*;
import java.util.List;


public interface RecensioneDao {
    /**
     * Cerca recensioni della struttura selezionata.
     * @param struttura id
     * @return Lista di recensioni
     * @throws SQLException se si verifica un errore di accesso al DB.
     */
    List<Recensione> cerca(String struttura) throws SQLException, ClassNotFoundException;


    /**
     * Ricerca con filtri nell'activity delle recensioni.
     * @param struttura id
     * @param valutazione stelle
     * @return Lista di Recensioni
     * @throws SQLException se si verifica un errore di accesso al DB.
     */
    List<Recensione> cerca(String struttura, String valutazione) throws SQLException, ClassNotFoundException;


    /**
     * Inserisce una nuova recensione scritta da visitatore.
     * @param visitatore autore
     * @param struttura oggetto della recensione
     * @param pubblicazione data
     * @param titolo titolo
     * @param testo recensione
     * @param valutazione gradimento
     * @throws SQLException se si verifica un errore di accesso al DB.
     */
    void inserisci(String visitatore, String struttura, String pubblicazione, String titolo, String testo, String valutazione) throws SQLException, ClassNotFoundException;


    /**
     * Popola la lista delle recensioni.
     * @param rs ResulSet
     * @return Lista Recensioni
     * @throws SQLException se viene chiamato su un ResultSet chiuso.
     */
    List<Recensione> getRecords(ResultSet rs) throws SQLException;
}
