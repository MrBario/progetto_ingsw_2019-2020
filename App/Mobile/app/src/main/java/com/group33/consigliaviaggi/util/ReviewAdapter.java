package com.group33.consigliaviaggi.util;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.group33.consigliaviaggi.R;
import com.group33.consigliaviaggi.model.entity.Recensione;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

// TODO: definire e verificare formattazione data dal database
public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ReviewHolder> implements Filterable {

    private static final String TAG = "ReviewAdapter";
    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    private List<Recensione> recensioni;
    private List<Recensione> recensioniFiltrate;
    private Context context;

    private final Comparator<Recensione> piuRecenti = new Comparator<Recensione>() {
        @Override
        public int compare(Recensione o1, Recensione o2) {
            Date d1, d2;
            try {
                d1 = formatter.parse(o1.getPubblicazione());
                d2 = formatter.parse(o2.getPubblicazione());
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e(TAG, "compare: Errore nel parsing delle date - " + e.getMessage());
                return 0;
            }
            return d1.compareTo(d2) * -1;
        }
    };

    private final Comparator<Recensione> menoRecenti = new Comparator<Recensione>() {
        @Override
        public int compare(Recensione o1, Recensione o2) {
            Date d1, d2;
            try {
                d1 = formatter.parse(o1.getPubblicazione());
                d2 = formatter.parse(o2.getPubblicazione());
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e(TAG, "compare: Errore nel parsing delle date - " + e.getMessage());
                return 0;
            }
            return d1.compareTo(d2);
        }
    };

    public void setRecensioni(Context context, final List<Recensione> recensioni) {
        this.context = context;
        if (this.recensioni == null) {
            this.recensioni = recensioni;
            this.recensioniFiltrate = new ArrayList<>();
            this.recensioniFiltrate.addAll(recensioni);
            notifyItemChanged(0, recensioniFiltrate.size());
        } else {
            final DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return ReviewAdapter.this.recensioni.size();
                }

                @Override
                public int getNewListSize() {
                    return recensioni.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return ReviewAdapter.this.recensioni.get(oldItemPosition).getId() == recensioni.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    return ReviewAdapter.this.recensioni.get(oldItemPosition).getId() == recensioni.get(newItemPosition).getId();
                }
            });
            this.recensioni = recensioni;
            this.recensioniFiltrate = recensioni;
            result.dispatchUpdatesTo(this);
        }
    }

    @NonNull
    @Override
    public ReviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.review_row, parent, false);
        return new ReviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewHolder holder, int position) {
        Recensione recensioneCorrente = recensioniFiltrate.get(position);
        holder.reviewTitle.setText(recensioneCorrente.getTitolo());
        holder.reviewText.setText(recensioneCorrente.getTesto());
        int value = recensioneCorrente.getValutazione();
        if (value <= holder.stars.length) {
            for (int i = 0; i < value; i++) { // Accendo le stelle pari alla valutazione
                holder.stars[i].setImageResource(android.R.drawable.btn_star_big_on);
            }
            if (value < holder.stars.length) {
                for (int i = value; i < holder.stars.length; i++) { // Spengo le restanti
                    holder.stars[i].setImageResource(android.R.drawable.btn_star_big_off);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: called");
        if (recensioni != null) {
            Log.d(TAG, "getItemCount: recensioniFiltrate: " + recensioniFiltrate);
            return (recensioniFiltrate != null) ? recensioniFiltrate.size() : 0;
        } else {
            return 0;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String valoreFiltro = constraint.toString();
                if (valoreFiltro.isEmpty()) {
                    recensioniFiltrate = recensioni;
                } else {
                    if (Constants.ASCENDING_ORDER_REQUEST.equalsIgnoreCase(valoreFiltro)) {
                        Log.d(TAG, "performFiltering: recensioniFiltrate before sort - " + recensioniFiltrate);
                        recensioniFiltrate.sort(menoRecenti);
                        Log.d(TAG, "performFiltering: recensioniFiltrate after sort - " + recensioniFiltrate);
                    } else if (Constants.DESCENDING_ORDER_REQUEST.equalsIgnoreCase(valoreFiltro)) {
                        Log.d(TAG, "performFiltering: recensioniFiltrate before sort - " + recensioniFiltrate);
                        recensioniFiltrate.sort(piuRecenti);
                        Log.d(TAG, "performFiltering: recensioniFiltrate after sort - " + recensioniFiltrate);
                    } else {
                        recensioniFiltrate = new ArrayList<>();
                        int voto = Integer.parseInt(valoreFiltro);
                        for (Recensione recensione : recensioni) {
                            if (recensione.getValutazione() >= voto) {
                                recensioniFiltrate.add(recensione);
                            }
                        }
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = recensioniFiltrate;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                recensioniFiltrate = (List<Recensione>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ReviewHolder extends RecyclerView.ViewHolder {

        TextView reviewTitle, reviewText;
        ImageView firstStar, secondStar, thirdStar, fourthStar, fifthStar;
        ImageView[] stars;

        public ReviewHolder(@NonNull View itemView) {
            super(itemView);
            reviewTitle = itemView.findViewById(R.id.reviewTitle);
            reviewText = itemView.findViewById(R.id.reviewText);
            firstStar = itemView.findViewById(R.id.firstStar);
            secondStar = itemView.findViewById(R.id.secondStar);
            thirdStar = itemView.findViewById(R.id.thirdStar);
            fourthStar = itemView.findViewById(R.id.fourthStar);
            fifthStar = itemView.findViewById(R.id.fifthStar);
            stars = new ImageView[]{firstStar, secondStar, thirdStar, fourthStar, fifthStar};
        }
    }
}
