package com.group33.consigliaviaggi.ui;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.group33.consigliaviaggi.R;
import com.group33.consigliaviaggi.model.dao.StrutturaDao;
import com.group33.consigliaviaggi.model.entity.Struttura;
import com.group33.consigliaviaggi.model.factory.StrutturaDaoFactory;
import com.group33.consigliaviaggi.util.DBUtil;
import com.group33.consigliaviaggi.util.VisitatoreCorrente;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.group33.consigliaviaggi.util.Constants.ERROR_DIALOG_REQUEST;
import static com.group33.consigliaviaggi.util.Constants.PERMISSIONS_REQUEST_ENABLE_GPS;
import static com.group33.consigliaviaggi.util.Constants.LOCATION_PERMISSION_REQUEST_CODE;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveStartedListener {

    private GoogleMap mMap;
    private View mapView;
    private Boolean mLocationPermissionsGranted = false;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Button user, reload;
    private Location currentLocation;
    private List<Struttura> elencoStrutture;
    private StrutturaDao Dao;
    private Circle circle;

    private boolean isMapMoving = false;

    private static final String TAG = "MapsActivity";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final float DEFAULT_ZOOM = 15f;
    private static final double DEFAULT_RANGE = 500;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Creazione del DAO:
        StrutturaDaoFactory DF = StrutturaDaoFactory.getDAOInstance(this);
        Dao = DF.getDAO();

        // Ottengo il SupportMapFragment e notifico quando la mappa e' pronta all'uso:
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapView = mapFragment.getView();


        // Set user button:
        user = findViewById(R.id.user_btn);
        user.setOnClickListener((v) -> {
            Intent intent;
            VisitatoreCorrente visitatoreCorrente = VisitatoreCorrente.getInstance();
            if(visitatoreCorrente.isLogged()) {
                intent = new Intent(MapsActivity.this, UserActivity.class);
            } else {
                intent = new Intent(MapsActivity.this, LoginActivity.class);
            }
            v.getContext().startActivity(intent);
        });

        SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar);
        final TextView seekBarValue = (TextView)findViewById(R.id.seekbarValue);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                seekBarValue.setText(String.valueOf(progress) + " m");
                mMap.clear();
                new AsyncLoad().execute();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        RatingBar ratingBar = (RatingBar)findViewById(R.id.ratingBar);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mMap.clear();
                new AsyncLoad().execute();
            }
        });

        reload = findViewById(R.id.reload_btn);
        reload.setOnClickListener(v -> {
            caricaStrutture();
            reload.setVisibility(View.INVISIBLE);
        });

        if (checkMapServices()) {
            getLocationPermission();
        }
    }

    private void initMap() {
        Log.d(TAG, "initMap: initializing map");
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapsActivity.this);
    }

    private boolean checkMapServices() {
        if (isServiceOk()) {
            return isGPSEnabled();
        }
        return false;
    }

    private boolean isServiceOk() {
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private boolean isGPSEnabled() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
            return false;
        }
        return true;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Questa applicazione necessita del GPS per funzionare, vuoi attivarlo?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Intent enableGpsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(enableGpsIntent, PERMISSIONS_REQUEST_ENABLE_GPS);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: called.");
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ENABLE_GPS: {
                if (mLocationPermissionsGranted) {
                    initMap();
                } else {
                    getLocationPermission();
                }
            }
        }
    }

    private void getDeviceLocation() {
        Log.d(TAG, "getDeviceLocation: getting the devices current location");

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            if (mLocationPermissionsGranted) {
                Task<Location> location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (checkMapServices()) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "onComplete: found location");
                                currentLocation = task.getResult();

                                if (currentLocation != null) {
                                    Log.d(TAG, "onComplete: found location null");
                                    moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                                            DEFAULT_ZOOM);
                                    // Una volta che ho la mia attuale posizione, carico le strutture intorno a me:
                                    caricaStrutture();
                                }
                            } else {
                                Log.d(TAG, "onComplete: current location is null");
                                Toast.makeText(MapsActivity.this, "Unable to get current location", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e(TAG, "getDeviceLocation: SecurityException" + e.getMessage());
        }
    }

    private void moveCamera(@org.jetbrains.annotations.NotNull LatLng latLng, float zoom) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    private void getLocationPermission() {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permissions = {FINE_LOCATION, COARSE_LOCATION};
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionsGranted = true;
                //initialize our map
                initMap();
            } else {
                ActivityCompat.requestPermissions(this,
                        permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionsGranted = false;

        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionsGranted = true;
                    //initialize our map
                    initMap();
                }
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);

        if (mLocationPermissionsGranted) {
            getDeviceLocation();

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            /* Spostiamo il bottone per riposizionarci sulle coordinate del device.
             * Di default si trova in alto a destra, ma tale posizione non va bene per il
             * layout scelto per la nostra app. */
            if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
                // Otteniamo la view del bottone
                View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                // e lo posizioniamo in basso a destra (come nell'app Google Maps)
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                // nelle successive righe modifichiamo effettivamente il layout del bottone:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                layoutParams.setMargins(0, 0, 30, 200);
            }
        }

        // Aggiungo un listner ai marker nella mappa:
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                // Creo una view modale con i dettagli del locale
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(MapsActivity.this, R.style.BottomSheetDialogTheme);
                View bottomSheetView = LayoutInflater.from(getApplicationContext())
                        .inflate(
                                R.layout.layout_bottom_sheet,
                                (ConstraintLayout) findViewById(R.id.bottomSheetContainer)
                        );
                //Recupero la struttura dal marker:
                Struttura struttura = elencoStrutture.get((Integer) marker.getTag());
                //Recupero e inizializzo gli elementi della view modale:
                TextView titolo = bottomSheetView.findViewById(R.id.review_btn);
                TextView media = bottomSheetView.findViewById(R.id.textViewBottom);
                TextView descrizione = bottomSheetView.findViewById(R.id.descrizione);

                titolo.setText(struttura.getNome());
                descrizione.setText(struttura.getDescrizione());
                media.setText((Math.floor(struttura.getMediaRecensioni() * 10) /10) + " / 5 Stelle");

                titolo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MapsActivity.this, ReviewsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("IDStruttura", struttura.getId());
                        intent.putExtra("nomeStruttura", struttura.getNome());
                        intent.putExtra("mediaRecStruttura", struttura.getMediaRecensioni());
                        v.getContext().startActivity(intent);
                        bottomSheetDialog.dismiss();
                    }
                });
                bottomSheetDialog.setContentView(bottomSheetView);
                bottomSheetDialog.show();
                return false;
            }
        });
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            isMapMoving = true;
        }
    }

    @Override
    public void onCameraIdle() {
        if (isMapMoving) {
            LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
            // TODO queste due istruzioni facevano impazzire il cerchio blu
            // currentLocation.setLatitude(bounds.northeast.latitude);
            //currentLocation.setLongitude(bounds.northeast.longitude);
            reload.setVisibility(View.VISIBLE);
            isMapMoving = false;
        }
    }

    private void caricaStrutture() {
        mMap.clear();
        new AsyncLoad().execute();
    }

    private class AsyncLoad extends AsyncTask<Void, Void, Void> {

        SeekBar seek = (SeekBar)findViewById(R.id.seekBar);
        RatingBar ratingBar = (RatingBar)findViewById(R.id.ratingBar);

        Circle circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()))
                .radius(seek.getProgress())
                .strokeColor(Color.BLUE)
                .fillColor(0x8800CCFF)
                .strokeWidth(2));

        @Override
        protected Void doInBackground(Void... voids) {

            if (currentLocation != null) {
                try {
                    DBUtil.getConnection();
                    Log.d(TAG, "doInBackground: Connessione Stabilita");
                    elencoStrutture = Dao.cerca(currentLocation.getLatitude(), currentLocation.getLongitude(), seek.getProgress(), (int)ratingBar.getRating());
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (elencoStrutture != null && !elencoStrutture.isEmpty()) {
                for (int i = 0; i < elencoStrutture.size(); i++) {
                    Struttura struttura = elencoStrutture.get(i);
                    LatLng position = new LatLng(struttura.getGeoLat(), struttura.getGeoLng());
                    Marker marker = mMap.addMarker(new MarkerOptions().position(position).title(struttura.getNome()));
                    marker.setTag(i);
                }
            }
        }
    }
}
