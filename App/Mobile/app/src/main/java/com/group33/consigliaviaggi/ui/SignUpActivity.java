package com.group33.consigliaviaggi.ui;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.group33.consigliaviaggi.R;
import com.group33.consigliaviaggi.model.dao.VisitatoreDao;
import com.group33.consigliaviaggi.model.entity.Visitatore;
import com.group33.consigliaviaggi.model.factory.VisitatoreDaoFactory;
import com.group33.consigliaviaggi.util.DBUtil;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SignUpActivity extends AppCompatActivity {

    private EditText nome, cognome, data, mail, nick, password;
    private String nomeValue, cognomeValue, dataValue, mailValue, nickValue, passwordValue, sesso;
    private RadioGroup sessoCheck;
    private Button sigup;

    private VisitatoreDao Dao;
    private AsyncRequest signUpRequest;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        nome = findViewById(R.id.nomeUtente);
        cognome = findViewById(R.id.cognomenomeUtente);
        data = findViewById(R.id.editTextDate);
        mail = findViewById(R.id.userMail);
        nick = findViewById(R.id.userNick);
        password = findViewById(R.id.userPassword);
        sigup = findViewById(R.id.signup_btn);
        sessoCheck = findViewById(R.id.sesso);
        initButton();

        // creazione del dao
        VisitatoreDaoFactory DF = VisitatoreDaoFactory.getDAOInstance(this);
        Dao = DF.getDAO();
        signUpRequest = new AsyncRequest();

    }

    private void initButton() {
        sigup.setOnClickListener((v) -> {
            nomeValue = String.valueOf(nome.getText());
            cognomeValue = String.valueOf(cognome.getText());
            dataValue = String.valueOf(data.getText());
            mailValue = String.valueOf(mail.getText());
            nickValue = String.valueOf(nick.getText());
            passwordValue = String.valueOf(password.getText());

            switch (sessoCheck.getCheckedRadioButtonId()) {
                case R.id.female_btn:
                    sesso = "F";
                    break;
                case R.id.male_btn:
                    sesso = "M";
                    break;
                default:
                    sesso = " ";
            }
            signUpRequest.execute();
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private class AsyncRequest extends AsyncTask<Void, Void, Void> {

        private static final String TAG = "AsyncTaskSignUp";

        @Override
        protected Void doInBackground(Void... voids) {

            final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");

            String d[] = dataValue.split("/");
            String data = LocalDate.of(Integer.parseInt(d[2]), Integer.parseInt(d[1]), Integer.parseInt(d[0])).format(formatter);

            try {
                DBUtil.getConnection();
                System.out.println("Connessione Stabilita");
                Dao.registrazione(nomeValue, cognomeValue, sesso, data, nickValue, mailValue, passwordValue);

            } catch (SQLException e) {
                Log.e(TAG, "doInBackground: Errore con la query -> " + e.getMessage());
                //e.printStackTrace();
                return null;
            } catch (ClassNotFoundException e) {
                Log.e(TAG, "doInBackground: Errore con la query -> " + e.getMessage());
                //e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            //TODO: Aggiornare il visitatore e fare login
            Log.d(TAG, "onPostExecute");
        }
    }
}