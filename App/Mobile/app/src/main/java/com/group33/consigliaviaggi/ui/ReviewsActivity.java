package com.group33.consigliaviaggi.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.group33.consigliaviaggi.R;
import com.group33.consigliaviaggi.model.dao.RecensioneDao;
import com.group33.consigliaviaggi.model.entity.Recensione;
import com.group33.consigliaviaggi.model.factory.RecensioneDaoFactory;
import com.group33.consigliaviaggi.util.Constants;
import com.group33.consigliaviaggi.util.DBUtil;
import com.group33.consigliaviaggi.util.ReviewAdapter;
import com.group33.consigliaviaggi.util.VisitatoreCorrente;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReviewsActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    private RecensioneDao Dao;
    private Integer id;
    private String nomeStruttura;
    private Float mediaRecStruttura;

    private FloatingActionButton addButton;
    private TextView struttura,mediaRec;
    private Spinner ordSpinner, valuSpinner;
    private ReviewAdapter reviewAdapter = null;

    List<Recensione> recensioni = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);

        // Recupero elementi della view:
        struttura = findViewById(R.id.struttura);
        mediaRec = findViewById(R.id.textViewReview);
        addButton = findViewById(R.id.add_btn);
        ordSpinner = findViewById(R.id.orderSpinner);
        valuSpinner = findViewById(R.id.voteSpinner);

        // Recupero dei dati passati tramite Intent:
        Bundle extras = getIntent().getExtras();
        id = extras.getInt("IDStruttura");
        nomeStruttura = extras.getString("nomeStruttura");
        mediaRecStruttura = extras.getFloat("mediaRecStruttura");

        // Setto nome struttura e media recensioni:
        struttura.setText(nomeStruttura);
        mediaRec.setText((Math.floor(mediaRecStruttura * 10) /10) + " / 5 Stelle");

        // Setto action del bottone:
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                VisitatoreCorrente visitatoreCorrente = VisitatoreCorrente.getInstance();
                if (visitatoreCorrente.isLogged()) {
                    intent = new Intent(ReviewsActivity.this, NewReviewActivity.class);
                    intent.putExtra("IDStruttura", id);
                    intent.putExtra("nomeStruttura", nomeStruttura);
                    intent.putExtra("mediaRecStruttura", mediaRecStruttura);
                } else {
                    intent = new Intent(ReviewsActivity.this, LoginActivity.class);
                }
                v.getContext().startActivity(intent);
            }
        });

        // Setto gli spinner:
        ordSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (reviewAdapter != null) {
                    switch (position) {
                        case 1:
                            reviewAdapter.getFilter().filter(Constants.DESCENDING_ORDER_REQUEST);
                            break;
                        case 2:
                            reviewAdapter.getFilter().filter(Constants.ASCENDING_ORDER_REQUEST);
                            break;
                        default:
                            //reviewAdapter.getFilter().filter(new String());
                            break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        valuSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (reviewAdapter != null) {
                    switch (position) {
                        case 1:
                            reviewAdapter.getFilter().filter("1");
                            break;
                        case 2:
                            reviewAdapter.getFilter().filter("2");
                            break;
                        case 3:
                            reviewAdapter.getFilter().filter("3");
                            break;
                        case 4:
                            reviewAdapter.getFilter().filter("4");
                            break;
                        case 5:
                            reviewAdapter.getFilter().filter("5");
                            break;
                        default:
                            reviewAdapter.getFilter().filter(new String());
                            break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        // creazione del dao
        RecensioneDaoFactory DF = RecensioneDaoFactory.getDAOInstance(this);
        Dao = DF.getDAO();

        // Setto la recyclerView:
        recyclerView = findViewById(R.id.reviewsView);
        recyclerView.setLayoutManager(new LinearLayoutManager(ReviewsActivity.this));
        reviewAdapter = new ReviewAdapter();
        recyclerView.setAdapter(reviewAdapter);

        // Recupero dati dal database:
        new Async().execute();
    }


    private class Async extends AsyncTask<Void, Void, Void> {

        private static final String TAG = "AsyncTask";

        @Override
        protected Void doInBackground(Void... voids) {
            ResultSet resultSet = null;
            try {
                DBUtil.getConnection();
                Log.d(TAG, "doInBackground: Connessione Stabilita");
                recensioni = Dao.cerca(id.toString());

            } catch (SQLException e) {
                Log.e(TAG, "doInBackground: Errore con la query -> " + e.getMessage());
                //e.printStackTrace();
                return null;
            } catch (ClassNotFoundException e) {
                Log.e(TAG, "doInBackground: Errore con la query -> " + e.getMessage());
                //e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d(TAG, "onPostExecute");
            reviewAdapter.setRecensioni(getApplicationContext(),recensioni);
        }
    }
}