package com.group33.consigliaviaggi.model.dao;

import com.group33.consigliaviaggi.model.entity.Struttura;
import com.group33.consigliaviaggi.model.entity.Visitatore;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface VisitatoreDao {
    /**
     * Registra un nuovo visitatore.
     * @param nome del visitatore.
     * @param cognome del visitatore.
     * @param sesso del visitatore.
     * @param nascita data.
     * @param nick del visitatore.
     * @param email ciro.esposito@example.com.
     * @throws SQLException se si verifica un errore di accesso al DB.
     */
    void registrazione(String nome, String cognome, String sesso, String nascita, String nick, String email, String password) throws SQLException, ClassNotFoundException;


    /**
     * Autenticazione visitatore.
     * @param email usata al momento della registrazione.
     * @param password usata al momento della registrazione.
     * @return istanza del visitatore autenticato.
     * @throws SQLException se si verifica un errore di accesso al DB o il visitatore non esiste.
     */
    List<Visitatore> login(String email, String password) throws SQLException, ClassNotFoundException;

    /**
     * Configura i campi del Visitatore se questo è registrato nel DB.
     * @param rs ResulSet
     * @return Il visitatore in una lista.
     * @throws SQLException se viene chiamato su un ResultSet chiuso.
     */
    List<Visitatore> getRecords(ResultSet rs) throws SQLException;
}
