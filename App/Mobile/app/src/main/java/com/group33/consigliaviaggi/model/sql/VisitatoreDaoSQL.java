package com.group33.consigliaviaggi.model.sql;

import com.group33.consigliaviaggi.model.dao.VisitatoreDao;
import com.group33.consigliaviaggi.model.entity.Struttura;
import com.group33.consigliaviaggi.model.entity.Visitatore;
import com.group33.consigliaviaggi.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VisitatoreDaoSQL implements VisitatoreDao {
    @Override
    public void registrazione(String nome, String cognome, String sesso, String nascita, String nick, String email, String password) throws SQLException, ClassNotFoundException {

        Pattern pat;
        Matcher matc;
        String sql = "INSERT INTO visitatori (";
        String sql2 = "VALUES(";

        if (!(nome.equals(""))) {
            sql += "nome,";
            sql2 += "'" + nome + "',";
        }
        if (!(cognome.equals(""))) {
            sql += "cognome,";
            sql2 += "'" + cognome + "',";
        }
        if (!(sesso.equals(""))) {
            sql += "sesso,";
            sql2 += "'" +sesso + "',";
        }
        if (!(nascita.equals(""))) {
            sql += "nascita,";
            sql2 += "'" + nascita + "',";
        }
        if (!(nick.equals(""))) {
            sql += "nick,";
            sql2 += "'" + nick + "',";
        }
        if (!(email.equals(""))) {
            sql += "email,";
            sql2 += "'" + email + "',";
        }
        if (!(password.equals(""))) {
            sql += "password";
            sql2 += "'" + password + "'";
        }

        pat = Pattern.compile(",$"); // cancella virgole finali
        matc = pat.matcher(sql);
        sql = matc.replaceAll("");
        matc = pat.matcher(sql2);
        sql2 = matc.replaceAll("");
        sql += ")" + sql2 + ")";

        try {
            DBUtil.dbExcecuteQuery(sql);
        } catch (SQLException e) {
            throw e;
        }
        catch (ClassNotFoundException c){
            throw c;
        }
    }

    @Override
    public List<Visitatore> login(String email, String password) throws SQLException, ClassNotFoundException{

        String sql = "SELECT * FROM visitatori WHERE email = '" + email + "' AND password = '" + password + "'";

        try{
            ResultSet rs = DBUtil.dbExcecute(sql);
            List<Visitatore> list = getRecords(rs);

            if(list.isEmpty()) throw new SQLException("email e password errati!");

            return list;
        }catch(SQLException e){
            throw e;
        }
        catch (ClassNotFoundException c){
            throw c;
        }
    }

    @Override
    public List<Visitatore> getRecords(ResultSet rs) throws SQLException {

        List<Visitatore> list = new ArrayList<>();

        try{
            while (rs.next()){
                Visitatore v = new Visitatore();
                v.setId(rs.getInt("ID_Visitatore"));
                v.setNome(rs.getString("Nome"));
                v.setCognome(rs.getString("Cognome"));
                v.setSesso(rs.getString("Sesso"));
                v.setNascita(rs.getString("Nascita"));
                v.setNick(rs.getString("Nick"));
                v.setEmail(rs.getString("Email"));
                v.setPassword(rs.getString("Password"));
                list.add(v);
            }
            return list;
        }catch(SQLException e){
            throw e;
        }
    }
}
