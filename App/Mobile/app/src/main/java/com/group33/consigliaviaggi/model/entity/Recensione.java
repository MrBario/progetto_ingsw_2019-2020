package com.group33.consigliaviaggi.model.entity;

public class Recensione {

    private int id;

    private String struttura;

    private String visitatore;

    private String pubblicazione;

    private String titolo;

    private String testo;

    private int valutazione;

    public void setId(int id) {
        this.id = id;
    }

    public void setStruttura(String struttura) {
        this.struttura = struttura;
    }

    public void setVisitatore(String visitatore) {
        this.visitatore = visitatore;
    }

    public void setValutazione(int valutazione) {
        this.valutazione = valutazione;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public void setTesto(String testo) {
        this.testo = testo;
    }

    public void setPubblicazione(String pubblicazione) {
        this.pubblicazione = pubblicazione;
    }

    public int getId() {
        return id;
    }

    public String getStruttura() {
        return struttura;
    }
    public String getVisitatore() {
        return visitatore;
    }

    public String getTitolo() {
        return titolo;
    }

    public String getTesto() {
        return testo;
    }

    public String getPubblicazione() {
        return pubblicazione;
    }

    public int getValutazione() {
        return valutazione;
    }

}
