package com.group33.consigliaviaggi.model.factory;

import android.content.Context;

import com.group33.consigliaviaggi.model.dao.VisitatoreDao;
import com.group33.consigliaviaggi.model.sql.VisitatoreDaoSQL;

import java.io.InputStream;
import java.util.Properties;

public class VisitatoreDaoFactory {

    String db;

    private static VisitatoreDaoFactory theDAO;

    public static synchronized VisitatoreDaoFactory getDAOInstance(Context ctx ) {
        if (theDAO == null)
            theDAO = new VisitatoreDaoFactory(ctx);

        return theDAO;

    }

    private VisitatoreDaoFactory(Context ctx) {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = ctx.getAssets().open("config.properties");
            prop.load(input);
            db = prop.getProperty("database");
            System.out.println(db);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public VisitatoreDao getDAO() {
        if (db.equals("sql"))
            return new VisitatoreDaoSQL();

        return null;
    }
}