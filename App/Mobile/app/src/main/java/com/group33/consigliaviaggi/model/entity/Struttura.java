package com.group33.consigliaviaggi.model.entity;

public class Struttura {

    private int id;

    private String nome;

    private String tipo;

    private String indirizzo;

    private double geoLat;

    private double geoLng;

    private String data;

    private String apertura;

    private String chiusura;

    private String listino;

    private String descrizione;

    private float mediaRecensioni;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public double getGeoLat() {
        return geoLat;
    }

    public double getGeoLng() {
        return geoLng;
    }

    public String getApertura() {
        return apertura;
    }

    public String getChiusura() {
        return chiusura;
    }

    public String getData() {
        return data;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public String getListino() {
        return listino;
    }

    public String getNome() {
        return nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setApertura(String apertura) {
        this.apertura = apertura;
    }

    public void setChiusura(String chiusura) {
        this.chiusura = chiusura;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public void setGeoLat(double geoLat) {
        this.geoLat = geoLat;
    }

    public void setGeoLng(double geoLng) {
        this.geoLng = geoLng;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public void setListino(String listino) {
        this.listino = listino;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getMediaRecensioni() {
        return mediaRecensioni;
    }

    public void setMediaRecensioni(float mediaRecensioni) {
        this.mediaRecensioni = mediaRecensioni;
    }
}
