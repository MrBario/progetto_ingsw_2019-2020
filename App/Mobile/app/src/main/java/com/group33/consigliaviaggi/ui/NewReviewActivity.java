package com.group33.consigliaviaggi.ui;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.group33.consigliaviaggi.R;
import com.group33.consigliaviaggi.model.dao.RecensioneDao;
import com.group33.consigliaviaggi.model.entity.Visitatore;
import com.group33.consigliaviaggi.model.factory.RecensioneDaoFactory;
import com.group33.consigliaviaggi.util.DBUtil;
import com.group33.consigliaviaggi.util.VisitatoreCorrente;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NewReviewActivity extends AppCompatActivity {

    private static final String TAG = "NewReviewActivity";
    private RecensioneDao Dao;
    private final SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
    private String nomeStruttura, idStruttura;


    private TextView struttura;
    private EditText titoloRecensione, corpoRecensione;
    private RadioGroup valutazione;
    private Button pubblicaRecensione;
    private String titoloR, corpoR, voto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_review);

        // Creazione del dao:
        RecensioneDaoFactory DF = RecensioneDaoFactory.getDAOInstance(this);
        Dao = DF.getDAO();

        // Recupero dei dati passati tramite Intent:
        Bundle extras = getIntent().getExtras();
        idStruttura = String.valueOf(extras.getInt("IDStruttura"));
        nomeStruttura = extras.getString("nomeStruttura");

        // Recupero e collego elementi della view alla classe:
        struttura = findViewById(R.id.struttura);
        titoloRecensione = findViewById(R.id.titoloRecensione);
        corpoRecensione = findViewById(R.id.corpoRecensione);
        valutazione = findViewById(R.id.voteGroup);
        pubblicaRecensione = findViewById(R.id.review_btn);

        // Inizializzo i campi della view relativi alla struttura:
        struttura.setText(nomeStruttura);

        // Inizializzo bottone:
        pubblicaRecensione.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titoloR = String.valueOf(titoloRecensione.getText());
                corpoR = String.valueOf(corpoRecensione.getText());

                switch (valutazione.getCheckedRadioButtonId()) {
                    case R.id.firstStar:
                        voto = "1";
                        break;
                    case R.id.secondStar:
                        voto = "2";
                        break;
                    case R.id.thirdStar:
                        voto = "3";
                        break;
                    case R.id.fourthStar:
                        voto = "4";
                        break;
                    case R.id.fifthStar:
                        voto = "5";
                        break;
                    default:
                        voto = "1";
                }

                // Pubblico la recensione:
                new AsyncRequest().execute();
            }
        });
    }

    private class AsyncRequest extends AsyncTask<Void, Void, Void> {

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                DBUtil.getConnection();
                Log.d(TAG, "doInBackground: Connessione Stabilita");
                VisitatoreCorrente visitatoreCorrente = VisitatoreCorrente.getInstance();
                Visitatore visitatore = visitatoreCorrente.getVisitatore();
                String pubblicazione = formatter.format(new Date());
                Dao.inserisci(String.valueOf(visitatore.getId()), idStruttura, pubblicazione, titoloR, corpoR, voto);

            } catch (SQLException e) {
                Log.e(TAG, "doInBackground: Errore con la query -> " + e.getMessage());
                //e.printStackTrace();
                buildAlertMessage();
                return null;
            } catch (ClassNotFoundException e) {
                Log.e(TAG, "doInBackground: Errore con la query -> " + e.getMessage());
                //e.printStackTrace();
                buildAlertMessage();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d(TAG, "onPostExecute: Pubblicata recensione");
            finish();
        }
    }

    private void buildAlertMessage() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Pubblicazione non riuscita, controlla di non aver lasciato campi vuoti")
                .setCancelable(true);
        final AlertDialog alert = builder.create();
        alert.show();
    }
}