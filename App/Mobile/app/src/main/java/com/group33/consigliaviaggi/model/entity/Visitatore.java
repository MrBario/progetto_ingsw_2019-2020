package com.group33.consigliaviaggi.model.entity;

public class Visitatore {

    private int id;

    private String nome;

    private String cognome;

    private String nascita;

    private String nick;

    private String sesso;

    private String email;

    private String password;

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCognome() {
        return cognome;
    }

    public String getEmail() {
        return email;
    }

    public String getNascita() {
        return nascita;
    }

    public String getNick() {
        return nick;
    }

    public String getPassword() {
        return password;
    }

    public String getSesso() {
        return sesso;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNascita(String nascita) {
        this.nascita = nascita;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }
}
