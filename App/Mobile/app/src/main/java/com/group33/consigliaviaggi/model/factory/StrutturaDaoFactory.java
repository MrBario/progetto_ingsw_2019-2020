package com.group33.consigliaviaggi.model.factory;

import android.content.Context;

import com.group33.consigliaviaggi.model.dao.StrutturaDao;
import com.group33.consigliaviaggi.model.sql.StrutturaDaoSQL;

import java.io.InputStream;
import java.util.Properties;

public class StrutturaDaoFactory {

    String db;

    private static StrutturaDaoFactory theDAO;

    public static synchronized StrutturaDaoFactory getDAOInstance(Context ctx) {
        if (theDAO == null)
            theDAO = new StrutturaDaoFactory(ctx);

        return theDAO;

    }

    private StrutturaDaoFactory(Context ctx) {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = ctx.getAssets().open("config.properties");
            prop.load(input);
            db = prop.getProperty("database");
            System.out.println(db);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public StrutturaDao getDAO() {
        if (db.equals("sql"))
            return new StrutturaDaoSQL();

        return null;
    }
}