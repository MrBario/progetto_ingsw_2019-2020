package com.group33.consigliaviaggi.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.group33.consigliaviaggi.R;
import com.group33.consigliaviaggi.model.entity.Visitatore;
import com.group33.consigliaviaggi.util.VisitatoreCorrente;

public class UserActivity extends AppCompatActivity {

    private VisitatoreCorrente visitatoreCorrente;
    private Visitatore visitatore;
    private TextView nome, cognome, mail, nick;
    private Button logOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        visitatoreCorrente = VisitatoreCorrente.getInstance();
        visitatore = visitatoreCorrente.getVisitatore();

        nome = findViewById(R.id.nome);
        cognome = findViewById(R.id.cognome);
        mail = findViewById(R.id.userMail);
        nick = findViewById(R.id.nick);

        if (visitatore != null) {
            nome.setText(visitatore.getNome());
            cognome.setText(visitatore.getCognome());
            mail.setText(visitatore.getEmail());
            nick.setText(visitatore.getNick());
        }

        logOut = findViewById(R.id.logout_btn);
        logOut.setOnClickListener(v -> {
            visitatoreCorrente.disconnectUser();
            finish();
        });
    }
}