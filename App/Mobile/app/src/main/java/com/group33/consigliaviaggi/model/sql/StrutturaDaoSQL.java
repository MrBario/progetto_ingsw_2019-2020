package com.group33.consigliaviaggi.model.sql;

import com.group33.consigliaviaggi.model.dao.StrutturaDao;
import com.group33.consigliaviaggi.model.entity.Recensione;
import com.group33.consigliaviaggi.model.entity.Struttura;
import com.group33.consigliaviaggi.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StrutturaDaoSQL implements StrutturaDao {

    @Override
    public List<Struttura> cerca(double geoLat, double geoLng, double range, int stelle) throws SQLException, ClassNotFoundException {

        double normalRange = 0.00001 * range;
        double boundN = geoLat + normalRange;
        double boundS = geoLat - normalRange;
        double boundW = geoLng - normalRange;
        double boundE = geoLng + normalRange;

        String sql = "SELECT * FROM votazione_strutture WHERE (Geolat BETWEEN " + boundS + "AND " + boundN + ") AND (Geolng BETWEEN " + boundW + "AND " + boundE +") AND Media >= " + stelle;

        try{
            ResultSet rs = DBUtil.dbExcecute(sql);
            List<Struttura> list = getRecords(rs);
            return list;
        }catch(SQLException e){
            throw e;
        }
        catch(ClassNotFoundException c){
            throw c;
        }
    }


    @Override
    public List<Struttura> getRecords(ResultSet rs) throws SQLException {
        try{
            List<Struttura> list = new ArrayList<>();

            if (rs != null) {
                while (rs.next()) {
                    Struttura s = new Struttura();
                    s.setId(rs.getInt("ID_Struttura"));
                    s.setNome(rs.getString("Nome"));
                    s.setTipo(rs.getString("Tipo"));
                    s.setIndirizzo(rs.getString("Indirizzo"));
                    s.setGeoLat(rs.getDouble("Geolat"));
                    s.setGeoLng(rs.getDouble("Geolng"));
                    s.setData(rs.getString("Data"));
                    s.setApertura(rs.getString("Apertura"));
                    s.setChiusura(rs.getString("Chiusura"));
                    s.setListino(rs.getString("Listino"));
                    s.setDescrizione(rs.getString("Descrizione"));
                    s.setMediaRecensioni(rs.getFloat("Media"));

                    list.add(s);
                }
            }
            return list;
        }
        catch(SQLException e){
            throw e;
        }
    }
}
