/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group33.model.entity;

import javafx.beans.property.*;

/**
 * Modello dei dati STATISTICHEVISITATORI
 *
 * @author MrBario
 */
public class StatisticheVisitatore {

    private IntegerProperty idProperty;

    private StringProperty nomeProperty;

    private StringProperty cognomeProperty;

    private StringProperty sessoProperty;

    private StringProperty nascitaProperty;

    private StringProperty nickProperty;

    private StringProperty emailProperty;

    private IntegerProperty oneStarProperty;

    private IntegerProperty twoStarsProperty;

    private IntegerProperty threeStarsProperty;

    private IntegerProperty fourStarsProperty;

    private IntegerProperty fiveStarsProperty;

    public StatisticheVisitatore() {
        idProperty = new SimpleIntegerProperty();
        nomeProperty = new SimpleStringProperty();
        cognomeProperty = new SimpleStringProperty();
        sessoProperty = new SimpleStringProperty();
        nascitaProperty = new SimpleStringProperty();
        nickProperty = new SimpleStringProperty();
        emailProperty = new SimpleStringProperty();
        oneStarProperty = new SimpleIntegerProperty();
        twoStarsProperty = new SimpleIntegerProperty();
        threeStarsProperty = new SimpleIntegerProperty();
        fourStarsProperty = new SimpleIntegerProperty();
        fiveStarsProperty = new SimpleIntegerProperty();
    }

    public int getId() {
        return idProperty.get();
    }

    public String getNome() {
        return nomeProperty.get();
    }

    public String getCognome() {
        return cognomeProperty.get();
    }

    public String getSesso() {
        return sessoProperty.get();
    }

    public String getNascita() {
        return nascitaProperty.get();
    }

    public String getNick() {
        return nickProperty.get();
    }

    public String getEmail() {
        return emailProperty.get();
    }

    public int getOneStar() {
        return oneStarProperty.get();
    }

    public int getTwoStars() {
        return twoStarsProperty.get();
    }

    public int getThreeStars() {
        return threeStarsProperty.get();
    }

    public int getFourStars() {
        return fourStarsProperty.get();
    }

    public int getFiveStars() {
        return fiveStarsProperty.get();
    }

    public void setId(int i) {
        idProperty.set(i);
    }

    public void setNome(String s) {
        nomeProperty.set(s);
    }

    public void setCognome(String s) {
        cognomeProperty.set(s);
    }

    public void setSesso(String s) {
        sessoProperty.set(s);
    }

    public void setNascita(String s) {
        nascitaProperty.set(s);
    }

    public void setNick(String s) {
        nickProperty.set(s);
    }

    public void setEmail(String s) {
        emailProperty.set(s);
    }

    public void setOneStar(int i) {
        oneStarProperty.set(i);
    }

    public void setTwoStars(int i) {
        twoStarsProperty.set(i);
    }

    public void setThreeStars(int i) {
        threeStarsProperty.set(i);
    }

    public void setFourStars(int i) {
        fourStarsProperty.set(i);
    }

    public void setFiveStars(int i) {
        fiveStarsProperty.set(i);
    }
}
