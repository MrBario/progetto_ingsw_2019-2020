/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group33.controller;

import com.group33.model.entity.StatisticheVisitatore;
import com.group33.model.factory.StatisticheVisitatoreDAOFactory;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.group33.model.dao.StatisticheVisitatoreDAO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.TextFieldTableCell;

import com.jfoenix.controls.*;

/**
 * FXML Controller class
 *
 * @author MrBario
 */
public class BackOfficeOperazioniStatisticheController implements Initializable {

    private StatisticheVisitatoreDAO Dao;

    // Campi Schermata
    @FXML
    private JFXTextField txtID;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private JFXTextField txtCognome;
    @FXML
    private CheckBox uomo;
    @FXML
    private CheckBox donna;
    @FXML
    private DatePicker dateNascita;
    @FXML
    private JFXTextField txtNick;
    @FXML
    private JFXTextField txtEmail;
    @FXML
    private TextArea console;
    @FXML
    private JFXButton aggiorna;
    @FXML
    private JFXButton elimina;
    @FXML
    private TableColumn<StatisticheVisitatore, Integer> idCol;
    @FXML
    private TableColumn<StatisticheVisitatore, String> nomeCol;
    @FXML
    private TableColumn<StatisticheVisitatore, String> cognomeCol;
    @FXML
    private TableColumn<StatisticheVisitatore, String> sessoCol;
    @FXML
    private TableColumn<StatisticheVisitatore, String> nascitaCol;
    @FXML
    private TableColumn<StatisticheVisitatore, String> nickCol;
    @FXML
    private TableColumn<StatisticheVisitatore, String> emailCol;
    @FXML
    private TableView<StatisticheVisitatore> tabella;
    @FXML
    private PieChart pie;

    public BackOfficeOperazioniStatisticheController() {
        StatisticheVisitatoreDAOFactory DF = StatisticheVisitatoreDAOFactory.getDAOInstance();

        Dao = DF.getDAO();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        /**
         * Personalizza per ogni colonna della tabella
         */
        nomeCol.setCellFactory(TextFieldTableCell.forTableColumn());
        nomeCol.setOnEditCommit(new EventHandler<CellEditEvent<StatisticheVisitatore, String>>() {

            @Override
            public void handle(CellEditEvent<StatisticheVisitatore, String> t) {
                String ripristina = tabella.getSelectionModel().getSelectedItem().getNome();
                ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                        .setNome(t.getNewValue());
                try {
                    modifica();
                } catch (SQLException e) {
                    ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                            .setNome(ripristina);
                    tabella.refresh();
                }
            }
        });
        cognomeCol.setCellFactory(TextFieldTableCell.forTableColumn());
        cognomeCol.setOnEditCommit(new EventHandler<CellEditEvent<StatisticheVisitatore, String>>() {
            @Override
            public void handle(CellEditEvent<StatisticheVisitatore, String> t) {
                String ripristina = tabella.getSelectionModel().getSelectedItem().getCognome();
                ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                        .setCognome(t.getNewValue());
                try {
                    modifica();
                } catch (SQLException e) {
                    ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                            .setCognome(ripristina);
                    tabella.refresh();
                }
            }
        });
        sessoCol.setCellFactory(TextFieldTableCell.forTableColumn());
        sessoCol.setOnEditCommit(new EventHandler<CellEditEvent<StatisticheVisitatore, String>>() {
            @Override
            public void handle(CellEditEvent<StatisticheVisitatore, String> t) {
                String ripristina = tabella.getSelectionModel().getSelectedItem().getSesso();
                ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                        .setSesso(t.getNewValue());
                try {
                    modifica();
                } catch (SQLException e) {
                    ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                            .setSesso(ripristina);
                    tabella.refresh();
                }
            }
        });
        nascitaCol.setCellFactory(TextFieldTableCell.forTableColumn());
        nascitaCol.setOnEditCommit(new EventHandler<CellEditEvent<StatisticheVisitatore, String>>() {
            @Override
            public void handle(CellEditEvent<StatisticheVisitatore, String> t) {
                String ripristina = tabella.getSelectionModel().getSelectedItem().getNascita();
                ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                        .setNascita(t.getNewValue());
                try {
                    modifica();
                } catch (SQLException e) {
                    ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                            .setNascita(ripristina);
                    tabella.refresh();
                }
            }
        });
        nickCol.setCellFactory(TextFieldTableCell.forTableColumn());
        nickCol.setOnEditCommit(new EventHandler<CellEditEvent<StatisticheVisitatore, String>>() {
            @Override
            public void handle(CellEditEvent<StatisticheVisitatore, String> t) {
                String ripristina = tabella.getSelectionModel().getSelectedItem().getNick();
                ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                        .setNick(t.getNewValue());
                try {
                    modifica();
                } catch (SQLException e) {
                    ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                            .setNick(ripristina);
                    tabella.refresh();
                }
            }
        });
        emailCol.setCellFactory(TextFieldTableCell.forTableColumn());
        emailCol.setOnEditCommit(new EventHandler<CellEditEvent<StatisticheVisitatore, String>>() {
            @Override
            public void handle(CellEditEvent<StatisticheVisitatore, String> t) {
                String ripristina = tabella.getSelectionModel().getSelectedItem().getEmail();
                ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                        .setEmail(t.getNewValue());
                try {
                    modifica();
                } catch (SQLException e) {
                    ((StatisticheVisitatore) t.getTableView().getItems().get(t.getTablePosition().getRow()))
                            .setEmail(ripristina);
                    tabella.refresh();
                }
            }
        });
    }

    /**
     * Metodo che si occupa dell'aggiornamento dei campi.
     */
    @FXML
    private void modifica() throws SQLException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");

        try {
            /*
             * Personalizzare le variabili per ogni tabella
             */
            StatisticheVisitatore s = tabella.getSelectionModel().getSelectedItem();
            String data = "";
            if (!(s.getNascita().equals(""))) {
                String d[] = s.getNascita().split("-");
                data = LocalDate.of(Integer.valueOf(d[0]), Integer.valueOf(d[1]), Integer.valueOf(d[2]))
                        .format(formatter);
            }

            Dao.modifica(String.valueOf(s.getId()), s.getNome(), s.getCognome(),
                    s.getSesso() == null ? "" : s.getSesso(), data, s.getNick(), s.getEmail());
            /*
             * Fine personalizzazione
             */
            eseguiOperazioniStatistiche();
            console.setStyle("");
            console.setText("Successo! Valori aggiornati correttamente nel DB");
        } catch (SQLException e) {
            console.setStyle("-fx-text-fill: red ;");
            console.setText("Errore! Eccezione lanciata durante l'aggiornamento dei dati nel DB:\n" + e);
            throw e;
        }

    }

    /**
     * Metodo che si occupa dell'eliminazione di un visitatore dalla tabella.
     */
    @FXML
    private void elimina() throws SQLException {
        try {
            if (txtID.getText().equals(""))
                throw new SQLException("Quale record vuoi eliminare? Inserisci l'ID ");

            Alert alert = new Alert(AlertType.CONFIRMATION, "Vuoi davvero eliminare permanentemente il record?",
                    ButtonType.YES, ButtonType.CANCEL);
            alert.setHeaderText("");
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                Dao.eliminaById(txtID.getText());
                console.setStyle("");
                console.setText("Successo! Record eliminato correttamente dal DB");
                tabella.getItems().remove(tabella.getSelectionModel().getSelectedItem());
            } else {
                console.setStyle("-fx-text-fill: red ;");
                console.setText("Operazione interrotta! Record non eliminato dal DB");
            }
        } catch (SQLException e) {
            console.setStyle("-fx-text-fill: red ;");
            console.setText("Errore! Eccezione lanciata durante l'eliminazione dei dati nel DB:\n" + e);
        }
    }

    /**
     * Metodo che si occupa di cercare i dati utili per le operazioni statistiche e mostrarli nella tabella.
     */
    @FXML
    private void cerca() throws SQLException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
        /*
         * Personalizzare le variabili per ogni tabella
         */
        String data = "";
        String d[] = dateNascita.getEditor().getText().split("/");
        String sesso = "";
        try {
            idCol.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue().getId()));
            nomeCol.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue().getNome()));
            cognomeCol.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue().getCognome()));
            sessoCol.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue().getSesso()));
            nascitaCol.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue().getNascita()));
            nickCol.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue().getNick()));
            emailCol.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue().getEmail()));

            if (dateNascita.getValue() != null)
                data = LocalDate.of(Integer.valueOf(d[2]), Integer.valueOf(d[1]), Integer.valueOf(d[0]))
                        .format(formatter);
            if (uomo.isSelected())
                sesso = "M";
            if (donna.isSelected())
                sesso = "F";

            ObservableList<StatisticheVisitatore> list = Dao.cerca(txtID.getText(), txtNome.getText(),
                    txtCognome.getText(), sesso, data, txtNick.getText(), txtEmail.getText());
            /*
             * Fine personalizzazione
             */
            popolaTabella(list);
        } catch (SQLException e) {
            console.setStyle("-fx-text-fill: red ;");
            console.setText("Errore! Eccezione lanciata durante la ricerca:\n" + e);
        }
    }

    private void popolaTabella(ObservableList<StatisticheVisitatore> list) {
        tabella.setItems(list);
        if (!(list.isEmpty())) {
            console.setStyle("");
            console.setText("Successo! La ricerca ha prodotto risultati"
                    + "\nNota: Per modificare fai doppio click sul campo desiderato");
        }

        else {
            console.setStyle("-fx-text-fill: red ;");
            console.setText("Errore! La Query non ha prodotto nessun risultato");
        }

    }

    /**
     * Resetta i campi della schermata.
     */
    @FXML
    private void resetta() {
        txtID.setText("");
        txtNome.setText("");
        txtCognome.setText("");
        uomo.setSelected(false);
        donna.setSelected(false);
        dateNascita.setValue(null);
        txtNick.setText("");
        txtEmail.setText("");
    }

    /**
     * Crea il diagramma a torta a partire dai dati recuperati nella ricerca.
     */
    @FXML
    private void eseguiOperazioniStatistiche() {
        StatisticheVisitatore v = tabella.getSelectionModel().getSelectedItem();
        if (v != null) {

            txtID.setText(String.valueOf(v.getId()));
            txtNome.setText(v.getNome());
            txtCognome.setText(v.getCognome());
            uomo.setSelected(v.getSesso().equals("M") ? true : false);
            donna.setSelected(v.getSesso().equals("F") ? true : false);
            dateNascita.setValue(LocalDate.parse(v.getNascita()));
            txtNick.setText(v.getNick());
            txtEmail.setText(v.getEmail());
            int totRec = v.getOneStar() + v.getTwoStars() + v.getThreeStars() + v.getFourStars() + v.getFiveStars();
            ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
                    new PieChart.Data("⋆", v.getOneStar() == 0 ? 0 : totRec / v.getOneStar() * 100),
                    new PieChart.Data("⋆⋆", v.getTwoStars() == 0 ? 0 : totRec / v.getTwoStars() * 100),
                    new PieChart.Data("⋆⋆⋆", v.getThreeStars() == 0 ? 0 : totRec / v.getThreeStars() * 100),
                    new PieChart.Data("⋆⋆⋆⋆", v.getFourStars() == 0 ? 0 : totRec / v.getFourStars() * 100),
                    new PieChart.Data("⋆⋆⋆⋆⋆", v.getFiveStars() == 0 ? 0 : totRec / v.getFiveStars() * 100));
            pie.setData(pieChartData);
        }

    }

}
