package com.group33.junit.test;

import java.sql.SQLException;

import com.group33.model.dao.StatisticheVisitatoreDAO;
import com.group33.model.entity.StatisticheVisitatore;
import com.group33.model.factory.StatisticheVisitatoreDAOFactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javafx.collections.ObservableList;

/**
 * Test class
 *
 * @author MrBario
 */
public class TestMethod {

    StatisticheVisitatoreDAO dao = StatisticheVisitatoreDAOFactory.getDAOInstance().getDAO();
    ObservableList<StatisticheVisitatore> list;

    /**
     * Risultati corretti per il record con id "2"
     */
    Integer id = Integer.valueOf(2);
    String nome = "Dario";
    String cognome = "Leone";
    String nascita = "1994-08-07";
    String sesso = "M";
    String nick = "MrBario";
    String email = "barius.leone@gmail.com";

    /**
     * Connessione al database e recuper dati.
     */
    @Before
    public void setup() {
        try {
            list = dao.cerca(id.toString(), "", "", "", "", "", "");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Confronto i dati recuperati con i risultati che dovrei avere.
     */
    @Test
    public void testId() {

        Assert.assertEquals(id.intValue(), list.get(0).getId());
    }

    @Test
    public void testNome() {

        Assert.assertEquals(nome, list.get(0).getNome());
    }

    @Test
    public void testCognome() {

        Assert.assertEquals(cognome, list.get(0).getCognome());
    }

    @Test
    public void testNascita() {

        Assert.assertEquals(nascita, list.get(0).getNascita());
    }

    @Test
    public void testSesso() {

        Assert.assertEquals(sesso, list.get(0).getSesso());
    }

    @Test
    public void testNick() {

        Assert.assertEquals(nick, list.get(0).getNick());

    }

    @Test
    public void testEmail() {

        Assert.assertEquals(email, list.get(0).getEmail());
    }

}