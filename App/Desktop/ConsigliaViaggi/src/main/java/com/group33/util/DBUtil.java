/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group33.util;
import java.sql.*;
import javax.sql.rowset.*;
import oracle.jdbc.pool.OracleDataSource;


/**
 *
 * @author MrBario
 */
public class DBUtil {
    
    /**
    * Indirizzo del server Oracle.
    */
   static public String host = "192.168.0.102";
   /**
    * Nome del servizio.
    */
   static public String servizio = "xe";
   /**
    * Porta utilizzata per la connessione.
    */
   static public int porta = 1337;
   /**
    * Nome utente per la connessione.
    */
   static public String user = "SYSTEM";
   /**
    * Password corrispondente all'utente specificato.
    */
   static public String password = "oracle";
   /**
    * Nome dello schema contenente le tabelle/viste/procedure cui si vuole
    * accedere; coincide di solito con il nome utente.
    */
   static public String schema = "LAB18";
   /**
    * Oggetto DataSource utilizzato nella connessione al DB
    */
   static private OracleDataSource ods;
   /**
    * Variabile che contiene la connessione attiva, se esiste
    */
   static private Connection defaultConnection;
    
    
    private static Connection connection = null;
    
    
    
    
    /**
    * Restituisce la connessione di default al DB.
    *
    * @return Connessione di default (quella gi&agrave; attiva, o una nuova
    * ottenuta in base ai parametri di connessione attualmente impostati
    * @throws SQLException In caso di problemi di connessione
    */
   static public Connection getDefaultConnection() throws SQLException {
      if (defaultConnection == null || defaultConnection.isClosed()) {
         defaultConnection = nuovaConnessione();
//         System.out.println("nuova connessione");
//      } else {
//         System.out.println("ricicla connessione");
      }

      return defaultConnection;
   }

   /**
    * Imposta una connessione specificata in input come default.
    *
    * @param c Connessione al DB
    */
   static public void setDefaultConnection(Connection c) {
      defaultConnection = c;
   }

   /**
    * Restituisce una nuova connessione al DB.
    *
    * @return Connessione al DB secondo i parametri attualmente impostati
    * @throws java.sql.SQLException in caso di problemi di connessione
    */
   static public Connection nuovaConnessione() throws SQLException {
      ods = new OracleDataSource();
      ods.setDriverType("thin");
      ods.setServerName(host);
      ods.setPortNumber(porta);
      ods.setUser(user);
      ods.setPassword(password);
      ods.setDatabaseName(servizio);
      return ods.getConnection();
   }

    
    
    public static void dbDisconnect() throws SQLException{
        try{
            if(connection != null && !connection.isClosed()){
                connection.close();
            }
        }
        catch(Exception e){
            throw e;
        }
    }
    
    
    // questo per insert/delete/update
    public static void dbExcecuteQuery(String sqlStmt) throws SQLException{
        Statement stmt = null;
        try{
            connection = getDefaultConnection();
            stmt = connection.createStatement();
            stmt.executeUpdate(sqlStmt);
        }
        catch(SQLException e){
            System.out.println("Errore durante dbExcecuteQuery" + e);
            throw e;
        }
        finally{
            if(stmt!=null){
                stmt.close();
            }
            dbDisconnect();
        }
    }

    // questo oer la ricerca nel database
    public static ResultSet dbExcecute(String sqlQuery) throws SQLException{
        Statement stmt = null;
        ResultSet rs = null;
        CachedRowSet crs = null;
        
        try{
            connection = getDefaultConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sqlQuery);
            crs = RowSetProvider.newFactory().createCachedRowSet();
            crs.populate(rs);
        }
        catch(SQLException e){
            System.out.println("Errore durante dbExcecute" + e);
            throw e;
        }
        finally{
            if(rs != null){
                rs.close();
            }
            if(stmt != null){
                stmt.close();
            }
            dbDisconnect();
        }
        return crs;
    }

    
}
