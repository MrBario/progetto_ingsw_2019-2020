/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group33.model.dao;

import java.sql.*;
import javafx.collections.ObservableList;
import com.group33.model.entity.StatisticheVisitatore;

/**
 *
 * @author MrBario
 */
public interface StatisticheVisitatoreDAO {

        /**
         * Update dei record nel DB.
         */
        void modifica(String id, String nome, String cognome, String sesso, String data, String nick, String email)
                        throws SQLException;


        /**
         * Delete dei record nel DB.
         */
        void eliminaById(String id) throws SQLException;


        /**
         * Retrieve dei record nel DB.
         */
        ObservableList<StatisticheVisitatore> cerca(String id, String nome, String cognome, String sesso, String data,
                        String nick, String email) throws SQLException;

        /**
         * Recupera i dati di ogni visitatore nei record.
         */
        ObservableList<StatisticheVisitatore> getRecords(ResultSet rs) throws SQLException;

}
