/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group33.model.factory;

import java.io.InputStream;
import java.util.Properties;

import com.group33.model.dao.StatisticheVisitatoreDAO;
import com.group33.model.sql.StatisticheVisitatoreDAOSql;

/**
 * Crea un StatiscticheVisitatoreDao in base al tipo di DB utilizzato.
 *
 * @author MrBario
 */
public class StatisticheVisitatoreDAOFactory {
	String db;

	private static StatisticheVisitatoreDAOFactory theDAO;

	public static synchronized StatisticheVisitatoreDAOFactory getDAOInstance() {
		if (theDAO == null)
			theDAO = new StatisticheVisitatoreDAOFactory();

		return theDAO;

	}

	private StatisticheVisitatoreDAOFactory() {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = getClass().getResourceAsStream("/com/group33/config/config.properties");
			prop.load(input);
			db = prop.getProperty("database");
			System.out.println(db);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public StatisticheVisitatoreDAO getDAO() {
		if (db.equals("sql"))
			return new StatisticheVisitatoreDAOSql();

		return null;
	}
}