/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group33.model.sql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.group33.util.DBUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import com.group33.model.dao.StatisticheVisitatoreDAO;
import com.group33.model.entity.StatisticheVisitatore;

/**
 * Implementazione di StatisticheVisitatoreDao per DB SQL.
 *
 * @author MrBario
 */
public class StatisticheVisitatoreDAOSql implements StatisticheVisitatoreDAO {

    @Override
    public ObservableList<StatisticheVisitatore> cerca(String id, String nome, String cognome, String sesso,
            String data, String nick, String email) throws SQLException {
        Pattern pat;
        Matcher matc;
        /*
         * Personalizzare i valori(UPPERCASE) per ogni tabella e le variabili stringa
         */
        String sql = "select * from STATISTICHE_VISITATORI where";

        if (!(id.equals(""))) {
            sql += " ID_VISITATORE = " + id + " and";
        }
        if (!(nome.equals(""))) {
            sql += " NOME = '" + nome + "' and";
        }
        if (!(cognome.equals(""))) {
            sql += " COGNOME = '" + cognome + "' and";
        }
        if (!(sesso.equals(""))) {
            sql += " SESSO = '" + sesso + "' and";
        }
        if (!(data.equals(""))) {
            sql += " NASCITA = '" + data + "' and";
        }
        if (!(nick.equals(""))) {
            sql += " NICK = '" + nick + "' and";
        }
        if (!(email.equals(""))) {
            sql += " EMAIL = '" + email + "'";
        }

        pat = Pattern.compile("where$|and$"); // cancella where o and finali
        matc = pat.matcher(sql);
        sql = matc.replaceAll("");
        sql += " order by ID_VISITATORE";
        /*
         * Fine personalizzazione
         */
        try {
            ResultSet rs = DBUtil.dbExcecute(sql);
            ObservableList<StatisticheVisitatore> list = getRecords(rs);
            return list;
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    public void modifica(String id, String nome, String cognome, String sesso, String data, String nick, String email)
            throws SQLException {
        /*
         * Personalizzare i valori(UPPERCASE) per ogni tabella e le variabili stringa
         */
        String sql = "update VISITATORI set  NOME = '" + nome + "', COGNOME = '" + cognome + "',  SESSO = '" + sesso
                + "', NASCITA = '" + data + "', NICK = '" + nick + "',  EMAIL = '" + email + "' where ID_VISITATORE ="
                + id;
        /*
         * Fine personalizzazione
         */

        try {
            DBUtil.dbExcecuteQuery(sql);
        } catch (SQLException e) {
            throw e;
        }

    }

    @Override
    public void eliminaById(String id) throws SQLException {
        /*
         * Personalizzare i valori(UPPERCASE) per ogni tabella e le variabili stringa
         */
        String sql = "delete from VISITATORI where ID_VISITATORE = " + id;
        /*
         * Fine personalizzazione
         */
        try {
            DBUtil.dbExcecuteQuery(sql);
        } catch (SQLException e) {
            throw e;
        }

    }

    @Override
    public ObservableList<StatisticheVisitatore> getRecords(ResultSet rs) throws SQLException {
        try {
            ObservableList<StatisticheVisitatore> list = FXCollections.observableArrayList();

            while (rs.next()) {
                /*
                 * Personalizzare i valori(UPPERCASE) per ogni tabella e le variabili stringa
                 */
                StatisticheVisitatore v = new StatisticheVisitatore();
                v.setId(rs.getInt("ID_VISITATORE"));
                v.setNome(rs.getString("NOME"));
                v.setCognome(rs.getString("COGNOME"));
                v.setSesso(rs.getString("SESSO"));
                v.setNascita(rs.getString("NASCITA"));
                v.setNick(rs.getString("NICK"));
                v.setEmail(rs.getString("EMAIL"));
                v.setOneStar(rs.getInt("ONESTAR"));
                v.setTwoStars(rs.getInt("TWOSTARS"));
                v.setThreeStars(rs.getInt("THREESTARS"));
                v.setFourStars(rs.getInt("FOURSTARS"));
                v.setFiveStars(rs.getInt("FIVESTARS"));
                /*
                 * Fine personalizzazione
                 */
                list.add(v);
            }

            return list;
        } catch (SQLException e) {
            throw e;
        }
    }

}