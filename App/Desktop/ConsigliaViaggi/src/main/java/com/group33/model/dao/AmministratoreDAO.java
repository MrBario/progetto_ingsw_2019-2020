/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group33.model.dao;

import java.sql.*;
import javafx.collections.ObservableList;
import com.group33.model.entity.Amministratore;

/**
 *
 * @author MrBario
 */

public interface AmministratoreDAO{

    /**
     * Autentica un amministratore in fase di login
     */
    ObservableList<Amministratore> controllaAmministratore(String matricola, String password) throws SQLException;

    /**
     * Recupera tutti i dati dell'amministratore appena loggato.
     */
    ObservableList<Amministratore> getRecords(ResultSet rs) throws SQLException;

}