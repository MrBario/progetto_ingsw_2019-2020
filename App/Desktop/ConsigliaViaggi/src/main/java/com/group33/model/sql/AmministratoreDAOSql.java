/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group33.model.sql;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.group33.util.DBUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import com.group33.model.dao.AmministratoreDAO;
import com.group33.model.entity.Amministratore;


/**
 * Implementazione di AmministratoreDao per DB SQL.
 *
 * @author MrBario
 */
public class AmministratoreDAOSql implements AmministratoreDAO {

    @Override
    public ObservableList<Amministratore> controllaAmministratore(String matricola, String password) throws SQLException{

        String sql = "SELECT * FROM Amministratori WHERE matricola = '" + matricola + "' AND password = '" + password + "'";
        
        try{
            ResultSet rs = DBUtil.dbExcecute(sql);
            ObservableList<Amministratore> list = getRecords(rs);

            if(list.isEmpty()) throw new SQLException("Campi non validi");

            return list;
        }catch(SQLException e){
            throw e;
        }
    }


    @Override
    public ObservableList<Amministratore> getRecords(ResultSet rs) throws SQLException {
        try {
            ObservableList<Amministratore> list = FXCollections.observableArrayList();

            while (rs.next()) {
                /*
                 * Personalizzare i valori(UPPERCASE) per ogni tabella e le variabili stringa
                 */
                Amministratore a = new Amministratore();
                a.setId(rs.getInt("ID_Amm"));
                a.setNome(rs.getString("Nome"));
                a.setCognome(rs.getString("Cognome"));
                a.setSesso(rs.getString("Sesso"));
                a.setNascita(rs.getString("Nascita"));
                a.setMat(rs.getString("Matricola"));
                /*
                 * Fine personalizzazione
                 */
                list.add(a);
            }

            return list;
        } catch (SQLException e) {
            throw e;
        }
    }
}