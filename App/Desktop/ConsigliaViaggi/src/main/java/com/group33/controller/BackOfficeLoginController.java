/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group33.controller;

import com.group33.model.factory.AmministratoreDAOFactory;
import com.group33.model.dao.AmministratoreDAO;

import java.io.IOException;
import java.sql.SQLException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import com.group33.util.*;
import com.jfoenix.controls.*;
import javafx.scene.image.Image;

/**
 * FXML Controller class
 * Classe che si occupa del Login dell'amministratore e della sua autenticazione.
 *
 * @author MrBario
 */
public class BackOfficeLoginController implements Initializable {

    private AmministratoreDAO Dao;

    // Campi Schermata
    @FXML
    private JFXTextField id;
    @FXML
    private JFXPasswordField password;
    @FXML
    private GridPane rootPane;


    public BackOfficeLoginController() {
        AmministratoreDAOFactory DF = AmministratoreDAOFactory.getDAOInstance();

        Dao = DF.getDAO();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        id.setText("");
        password.setText("");
        id.setFocusTraversable(false);
        password.setFocusTraversable(false);
    }

    /**
     * Prova ad accedere al database con i campi ID e Password.
     * Se riesce mostra la schermata principale, altrimenti mostra
     * un messaggio d'errore.
     */
    @FXML
    private void accedi(ActionEvent event) {
        try {
            if (id.getText().equals("") || password.getText().equals(""))
                throw new SQLException("Campi non validi");

            DBUtil.setDefaultConnection(DBUtil.nuovaConnessione());
            System.out.println("Connessione Stabilita");

            // Auteticazione Amministratore
            Dao.controllaAmministratore(id.getText(), password.getText());

            // Chiude la schermata di login
            rootPane.getScene().getWindow().hide();

            mostraSchermataPrincipale();

        } catch (Exception e) {
            mostraErrore("Connessione Fallita: " + e, "Errore", null);
            e.printStackTrace();
        }
    }

    /**
     * Carica il file FXML corrispondente alla schermata principale.
     */
    private void mostraSchermataPrincipale() {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/com/group33/view/SchermataPrincipale.fxml"));
            // apre la schermata principale
            Stage stage = new Stage();
            stage.setScene(new Scene(root, 900, 600));

            stage.setTitle("Consiglia Viaggi 2019");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/group33/images/icon.png")));
            stage.setResizable(true);
            stage.show();

        } catch (IOException e) {
            mostraErrore("La schermata non può essere caricata " + e, "Errore", "IOException");
            e.printStackTrace();
        }

    }

    /**
     * Traduce gli errori dal Database e li mostra 
     * in un formato comprendibile all'amministratore.
     */
    public static void mostraErrore(String infoMessage, String titleBar, String headerMessage) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(titleBar);
        alert.setHeaderText(headerMessage);
        alert.setContentText(infoMessage);
        alert.showAndWait();
    }

}
