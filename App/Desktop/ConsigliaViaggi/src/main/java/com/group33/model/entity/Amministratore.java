/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group33.model.entity;

import javafx.beans.property.*;

/**
 *  Modello dei dati AMMINISTRATORI
 * 
 * @author MrBario
 */
public class Amministratore {

    private IntegerProperty idProperty;

    private StringProperty nomeProperty;

    private StringProperty cognomeProperty;

    private StringProperty sessoProperty;

    private StringProperty nascitaProperty;

    private StringProperty matProperty;

    public Amministratore() {
        idProperty = new SimpleIntegerProperty();
        nomeProperty = new SimpleStringProperty();
        cognomeProperty = new SimpleStringProperty();
        sessoProperty = new SimpleStringProperty();
        nascitaProperty = new SimpleStringProperty();
        matProperty = new SimpleStringProperty();
    }

    public int getId() {
        return idProperty.get();
    }

    public String getNome() {
        return nomeProperty.get();
    }

    public String getCognome() {
        return cognomeProperty.get();
    }

    public String getSesso() {
        return sessoProperty.get();
    }

    public String getNascita() {
        return nascitaProperty.get();
    }

    public String getMat() {
        return matProperty.get();
    }


    public void setId(int i) {
        idProperty.set(i);
    }

    public void setNome(String s) {
        nomeProperty.set(s);
    }

    public void setCognome(String s) {
        cognomeProperty.set(s);
    }

    public void setSesso(String s) {
        sessoProperty.set(s);
    }

    public void setNascita(String s) {
        nascitaProperty.set(s);
    }

    public void setMat(String s) {
        matProperty.set(s);
    }
}