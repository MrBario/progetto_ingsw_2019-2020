/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group33.model.factory;

import java.io.InputStream;
import java.util.Properties;

import com.group33.model.dao.AmministratoreDAO;
import com.group33.model.sql.AmministratoreDAOSql;

/**
 * Crea un AmministratoreDao in base al tipo di DB utilizzato.
 *
 * @author MrBario
 */
public class AmministratoreDAOFactory {
	String db;

	private static AmministratoreDAOFactory theDAO;

	public static synchronized AmministratoreDAOFactory getDAOInstance() {
		if (theDAO == null)
			theDAO = new AmministratoreDAOFactory();

		return theDAO;

	}

	private AmministratoreDAOFactory() {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = getClass().getResourceAsStream("/com/group33/config/config.properties");
			prop.load(input);
			db = prop.getProperty("database");
			System.out.println(db);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public AmministratoreDAO getDAO() {
		if (db.equals("sql"))
			return new AmministratoreDAOSql();

		return null;
	}
}