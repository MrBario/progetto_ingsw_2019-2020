module main{
    requires javafx.controls;
    requires javafx.fxml;
    requires transitive javafx.graphics;
    requires oracle;
    requires com.jfoenix;
    requires java.sql.rowset;
    requires java.sql;
    requires junit;
    opens com.group33.controller to javafx.fxml;
    exports com.group33.main;
}