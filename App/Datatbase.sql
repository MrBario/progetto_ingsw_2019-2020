/**
* TABELLA: AMMINISTRATORI
* Crea la tabella e implementa i vincoli piu' semplici.
*/
-- Crea la tabella AMMINISTRATORI

CREATE TABLE AMMINISTRATORI
(
ID_Amm INTEGER DEFAULT '0' NOT NULL,
Nome VARCHAR2(64) NOT NULL,
Cognome VARCHAR2(64) NOT NULL,
Sesso CHAR(1),
Nascita DATE NOT NULL,
Matricola VARCHAR(64) NOT NULL,
Password VARCHAR2(256) NOT NULL
);
/
ALTER TABLE VISITATORI
ADD(
CONSTRAINT Amministratore_pk PRIMARY KEY(ID_Visitatore),
CONSTRAINT SessoAmm CHECK (UPPER(Sesso) IN ('M','F'))
);

/

-- Trigger per settare, se necessario, la chiave primaria automaticamente
 CREATE OR REPLACE TRIGGER AmministratoriPK
 BEFORE INSERT ON AMMINISTRATORI
 FOR EACH ROW
 BEGIN
    DECLARE
        pk AMMINISTRATORI.ID_Amm%TYPE;
    BEGIN
        IF(:NEW.ID_Amm = 0) THEN
            SELECT NVL(MAX(ID_Amm),0) + 1 INTO pk FROM AMMINISTRATORI;
            :NEW.ID_Amm := pk;
        END IF;
    END;
END;
/


/**
* TABELLA: VISITATORI
* Crea la tabella e implementa i vincoli piu' semplici.
*/
-- Crea la tabella VISITATORI

CREATE TABLE VISITATORI
(
ID_Visitatore INTEGER DEFAULT '0' NOT NULL,
Nome VARCHAR2(64) NOT NULL,
Cognome VARCHAR2(64) NOT NULL,
Sesso CHAR(1),
Nascita DATE NOT NULL,
Nick VARCHAR(64) NOT NULL,
eMail VARCHAR2(320) NOT NULL UNIQUE CHECK(eMail LIKE '_%@_%.__%'),
-- una email legittima puo' contare {64}@{255} caratteri.
-- In totale 320.
Password VARCHAR2(256) NOT NULL
);

/
ALTER TABLE VISITATORI
ADD(
CONSTRAINT Visitatore_pk PRIMARY KEY(ID_Visitatore),
CONSTRAINT SessoVisitatore CHECK (UPPER(Sesso) IN ('M','F'))
);

/

-- Trigger per settare, se necessario, la chiave primaria automaticamente
 CREATE OR REPLACE TRIGGER VisitatoriPK
 BEFORE INSERT ON VISITATORI
 FOR EACH ROW
 BEGIN
    DECLARE
        pk VISITATORI.ID_Visitatore%TYPE;
    BEGIN
        IF(:NEW.ID_Visitatore = 0) THEN
            SELECT NVL(MAX(ID_Visitatore),0) + 1 INTO pk FROM VISITATORI;
            :NEW.ID_Visitatore := pk;
        END IF;
    END;
END;

/
/**
* TABELLA: STRUTTURE
* Crea la tabella e implementa i vincoli piu' semplici.
*/
-- Crea la tabella STRUTTURE

CREATE TABLE STRUTTURE
(
ID_Struttura INTEGER DEFAULT '0' NOT NULL,
Nome VARCHAR2(64) NOT NULL,
Tipo VARCHAR2 (10) NOT NULL,
Indirizzo VARCHAR2(128) NOT NULL,
Geolat DECIMAL(10,7) NOT NULL,
Geolng DECIMAL(10,7) NOT NULL,
Data DATE DEFAULT SYSDATE,
Apertura DATE,
Chiusura DATE,
Listino VARCHAR2(200) DEFAULT NULL,
Descrizione VARCHAR2(100) DEFAULT NULL

);

/

-- Crea il vincolo di chiave primaria
ALTER TABLE STRUTTURE
ADD(
CONSTRAINT Strutture_pk PRIMARY KEY(ID_Struttura),
CONSTRAINT TipoStruttura CHECK (UPPER(Tipo) IN ('HOTEL','RISTORANTE', 'EVENTO'))
);

/

-- Trigger per settare, se necessario, la chiave primaria automaticamente
 CREATE OR REPLACE TRIGGER StrutturePK
 BEFORE INSERT ON STRUTTURE
 FOR EACH ROW
 BEGIN
    DECLARE
        pk STRUTTURE.ID_Struttura%TYPE;
    BEGIN
        IF(:NEW.ID_Struttura = 0) THEN
            SELECT NVL(MAX(ID_Struttura),0) + 1 INTO pk FROM STRUTTURE;
            :NEW.ID_Struttura := pk;
        END IF;
    END;
END;

/

/**
* TABELLA: RECENSIONI
* Crea la tabella e implementa i vincoli piu' semplici.
*/
-- Crea la tabella RECENSIONI

CREATE TABLE RECENSIONI
(
ID_Recensione INTEGER DEFAULT '0' NOT NULL,
Visitatore INTEGER NOT NULL,
Struttura INTEGER NOT NULL,
Pubblicazione DATE DEFAULT SYSDATE NOT NULL,
Titolo VARCHAR2(64) NOT NULL,
Testo VARCHAR2(1000) NOT NULL,
Valutazione INTEGER NOT NULL
);

/

-- Crea il vincolo di chiave primaria
ALTER TABLE RECENSIONI
ADD (
CONSTRAINT Recensioni_pk PRIMARY KEY(ID_Recensione),
CONSTRAINT RECENSIONI_fk FOREIGN KEY(Visitatore) REFERENCES VISITATORI(ID_Visitatore) ON DELETE CASCADE,
CONSTRAINT RECENSIONI_fk2 FOREIGN KEY(Struttura) REFERENCES STRUTTURE(ID_Struttura) ON DELETE CASCADE
);


/

-- Trigger per settare, se necessario, la chiave primaria automaticamente
 CREATE OR REPLACE TRIGGER RecensioniPK
 BEFORE INSERT ON RECENSIONI
 FOR EACH ROW
 BEGIN
    DECLARE
        pk RECENSIONI.ID_Recensione%TYPE;
    BEGIN
        IF(:NEW.ID_Recensione = 0) THEN
            SELECT NVL(MAX(ID_Recensione),0) + 1 INTO pk FROM RECENSIONI;
            :NEW.ID_Recensione := pk;
        END IF;
    END;
END;

/
CREATE OR REPLACE VIEW VOTAZIONE_STRUTTURE
AS
SELECT s.id_struttura, s.nome, s.tipo, s.indirizzo, s.geolat, s.geolng, s.data, s.apertura, s.chiusura, s.listino, s.descrizione, AVG(R.Valutazione) AS Media
FROM STRUTTURE S JOIN RECENSIONI R ON R.struttura = S.Id_Struttura
GROUP BY S.Id_struttura, s.nome, s.tipo, s.indirizzo, s.geolat, 
s.geolng, s.data, s.apertura, s.chiusura, s.listino, 
s.descrizione;
/

CREATE OR REPLACE VIEW STATISTICHE_VISITATORI 
AS
SELECT V.ID_Visitatore, V.Nome, V.Cognome, V.Sesso, V.Nascita, V.Nick, V.Email, A.onestar, B.twostars, C.threestars, D.fourstars, E.fivestars
FROM VISITATORI V
    LEFT OUTER JOIN(SELECT R.Visitatore, COUNT(*) AS OneStar
         FROM RECENSIONI R
         WHERE R.Valutazione = 1
         GROUP BY R.Visitatore) A ON V.ID_Visitatore = A.Visitatore 
    LEFT OUTER JOIN (SELECT R.Visitatore, COUNT(*) AS TwoStars
         FROM RECENSIONI R
         WHERE R.Valutazione = 2
         GROUP BY R.Visitatore) B ON V.ID_Visitatore = B.Visitatore 
    LEFT OUTER JOIN (SELECT R.Visitatore, COUNT(*) AS ThreeStars
         FROM RECENSIONI R
         WHERE R.Valutazione = 3
         GROUP BY R.Visitatore) C ON V.ID_Visitatore = C.Visitatore 
    LEFT OUTER JOIN (SELECT R.Visitatore, COUNT(*) AS FourStars
         FROM RECENSIONI R
         WHERE R.Valutazione = 4
         GROUP BY R.Visitatore) D ON V.ID_Visitatore = D.Visitatore 
    LEFT OUTER JOIN (SELECT R.Visitatore, COUNT(*) AS FiveStars
         FROM RECENSIONI R
         WHERE R.Valutazione = 5
         GROUP BY R.Visitatore) E ON V.ID_Visitatore = E.Visitatore 
ORDER BY V.ID_Visitatore;

/

--POPOLA VISITATORI PER TEST
INSERT INTO VISITATORI(ID_Visitatore, Nome, Cognome, Sesso, Nascita, Nick, eMail, Password)
VALUES(1, 'Nilla', 'Buonocore', 'F', TO_DATE('27-09-1990','DD-MM-YYYY'), 'Melannurca', 'buonocore.petronilla@gmail.com', 'ciao');
/
INSERT INTO VISITATORI(ID_Visitatore, Nome, Cognome, Sesso, Nascita, Nick, eMail, Password)
VALUES(2, 'Dario', 'Leone', 'M', TO_DATE('7-08-1994','DD-MM-YYYY'), 'MrBario', 'barius.leone@gmail.com', 'banana');
/
INSERT INTO VISITATORI(ID_Visitatore, Nome, Cognome, Sesso, Nascita, Nick, eMail, Password)
VALUES(3, 'Amerigo', 'Buonocore', 'M', TO_DATE('14-04-1988','DD-MM-YYYY'), 'Ame', 'amerigo.buonocore@gmail.com', 'picanha');
/
--POPOLA STRUTTURE PER TEST
INSERT INTO STRUTTURE(id_struttura, Nome, Tipo, Indirizzo, Geolat, Geolng, Apertura, Chiusura, Listino, Descrizione)
VALUES(1, 'A FETTINA DO ZZU TONINU', 'RISTORANTE', 'Via Bausan, 16A, 88100 Catanzaro CZ', 38.8195646, 16.6126554, to_date('12:30','hh:mi'), to_date('01:00','hh:mi'), 'Arrosto 5 euro', 'Ristorante di Tonino');
/
--POPOLA RECENSIONI PER TEST
INSERT INTO RECENSIONI(ID_Recensione, Visitatore, Struttura, Pubblicazione, Titolo, Testo, Valutazione)
VALUES(1, 1, 1, SYSDATE, 'one', 'star', 1);
/
INSERT INTO RECENSIONI(ID_Recensione, Visitatore, Struttura, Pubblicazione, Titolo, Testo, Valutazione)
VALUES(2, 1, 1, SYSDATE, 'two', 'stars', 2);
/
INSERT INTO RECENSIONI(ID_Recensione, Visitatore, Struttura, Pubblicazione, Titolo, Testo, Valutazione)
VALUES(3, 1, 1, SYSDATE, 'three', 'stars', 3);
/
INSERT INTO RECENSIONI(ID_Recensione, Visitatore, Struttura, Pubblicazione, Titolo, Testo, Valutazione)
VALUES(4, 1, 1, SYSDATE, 'four', 'stars', 4);
/
INSERT INTO RECENSIONI(ID_Recensione, Visitatore, Struttura, Pubblicazione, Titolo, Testo, Valutazione)
VALUES(5, 1, 1, SYSDATE, 'Mangiato Bene', 'Carne al sangue come piace a me', 5);
/

INSERT INTO RECENSIONI(ID_Recensione, Visitatore, Struttura, Pubblicazione, Titolo, Testo, Valutazione)
VALUES(6, 2, 1, SYSDATE, 'one', 'star', 2);
/
INSERT INTO RECENSIONI(ID_Recensione, Visitatore, Struttura, Pubblicazione, Titolo, Testo, Valutazione)
VALUES(7, 2, 1, SYSDATE, 'two', 'stars', 2);
/
INSERT INTO RECENSIONI(ID_Recensione, Visitatore, Struttura, Pubblicazione, Titolo, Testo, Valutazione)
VALUES(8, 2, 1, SYSDATE, 'three', 'stars', 3);
/
INSERT INTO RECENSIONI(ID_Recensione, Visitatore, Struttura, Pubblicazione, Titolo, Testo, Valutazione)
VALUES(9, 2, 1, SYSDATE, 'four', 'stars', 4);
/
INSERT INTO RECENSIONI(ID_Recensione, Visitatore, Struttura, Pubblicazione, Titolo, Testo, Valutazione)
VALUES(10, 2, 1, SYSDATE, 'Mangiato Bene', 'Carne al sangue come piace a me', 5);
/
INSERT INTO AMMINISTRATORI VALUES (1,'Dario','Leone', 'M', TO_DATE(SYSDATE),'n86002202', 'abcd1234');
/
SELECT * FROM Amministratori WHERE matricola ='n86002202' AND password = 'abcd1234';
/*--DROP table in production to wakeup faster in the morning
DROP TABLE RECENSIONI;
/
DROP TABLE STRUTTURE;
/
DROP TABLE VISITATORI;

*/